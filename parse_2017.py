import csv
import json
import pathlib
import urllib


CRIMES = (
    'Murder',
    'Manslaughter',
    'Rape',
    'Sodomy',
    'Sexual Assault w/Object',
    'Fondling',
    'Aggravated Assault',
    'Simple Assault',
    'Intimidation',
    'Kidnapping',
    'Incest',
    'Statutory Rape',
    'Human Trafficking Offenses',
    'Violation of No Contact/Protect.',
    'Robbery',
    'Burglary',
    'Larceny-Theft Offenses',
    'Motor Vehicle Theft',
    'Arson',
    'Destruction of Property',
    'Counterfeiting/Forgery',
    'Fraud Offenses',
    'Embezzlement',
    'Extortion/Blackmail',
    'Bribery',
    'Stolen Property Offenses',
    'Animal Cruelty',
    'Drug/Narcotic Violations',
    'Drug Equipment Violations',
    'Gambling Offenses',
    'Pornography',
    'Prostitution Offenses',
    'Weapon Law Violations',
)

START = 91
END = 544
P = lambda p: (p - (START + 1)) // 2

TITLES = {
    P(512): dict(
        agency='Colfax PD',
        county='Whitman County',
        population='2795',
        months='1',
    ),
    P(522): dict(
        agency='Granger PD',
        county='Yakima County',
        population='3905',
        months='6',
    ),
    P(528): dict(
        agency='Selah PD',
        county='Yakima County',
        population='7630',
        months='12',
    ),
    P(532): dict(
        agency='Tieton PD',
        county='Yakima County',
        population='1300',
        months='9',
    ),
    P(420): dict(
        agency='Index PD',
        county='Snohomish County',
        population='175',
        months='11',
    ),
    P(444): dict(
        agency='Spokane SO',
        county='Spokane County',
        population='157360',
        months='12',
    ),
    P(454): dict(
        agency='Spokane PD',
        county='Okanogan County',
        population='217300',
        months='12',
    ),
    P(456): dict(
        agency='Spokane Valley PD',
        county='Spokane County',
        population='94890',
        months='12',
    ),
    P(324): dict(
        agency='Brewster PD',
        county='Okanogan County',
        population='2400',
        months='11',
    ),
    P(326): dict(
        agency='Coulee Dam PD',
        county='Okanogan County',
        population='1100',
        months='11',
    ),
    P(328): dict(
        agency='Omak PD',
        county='Okanogan County',
        population='4925',
        months='6',
    ),
    P(332): dict(
        agency='Tonasket PD',
        county='Okanogan County',
        population='1110',
        months='5',
    ),
    P(286): dict(
        agency='Kittitas PD',
        county='Kittitas County',
        population='1500',
        months='9',
    ),
    P(266): dict(
        agency='Yarrow Point PD',
        county='King County',
        population='1040',
        months='11',
    ),
    P(206): dict(
        agency='Island SO',
        county='Island County',
        population='56880',
        months='9',
    ),
    P(204): dict(
        agency='Westport PD',
        county='Grays Harbor County',
        population='2115',
        months='11',
    ),
    P(196): dict(
        agency='Hoquiam PD',
        county='Grays Harbor County',
        population='8560',
        months='12',
    ),
    P(120): dict(
        agency='Forks PD',
        county='Clallam County',
        population='3595',
        months='12',
    ),
    P(136): dict(
        agency='Vancouver PD',
        county='Clark County',
        population='181928',
        months='12',
    ),
    P(306): dict(
        agency='Napavine PD',
        county='Lewis County',
        population='1900',
        months='12',
    ),
    P(100): dict(
        agency='Asotin PD',
        county='Asotin County',
        population='1275',
        months='8',
    ),
}


def parse_report(report, index):
    grid = {}
    for t in report['Texts']:
        x = round(t['x'], 2)
        y = round(t['y'], 2)

        grid[y] = grid.get(y, {})
        grid[y][x] = urllib.parse.unquote(t['R'][0]['T']).strip()

    rows = [
        [grid[y][x] for x in sorted(grid[y].keys())]
        for y in sorted(grid.keys())
    ]

    crimes = {
        row[0]: row[2].replace(',', '')
        for row in rows
        if row[0] in CRIMES
    }

    try:
        return dict(
            crimes,
            year='2017',
            **TITLES.get(index, dict(
                agency=rows[1][0],
                county=rows[0][0],
                population=rows[1][1].replace(',', ''),
                months=rows[2][1],
            )),
        )
    except IndexError:
        print(index, rows[0:20])
        raise


reports = json.loads(
    pathlib.Path('decomposed/2017.json').read_text()
)['formImage']['Pages'][START:END:2]


with open('converted/2017.csv', 'w') as csvfile:
    writer = csv.DictWriter(csvfile, fieldnames=(
        'agency',
        'county',
        'population',
        'months',
        'year',
    ) + CRIMES)

    writer.writeheader()
    for i, report in enumerate(reports):
        writer.writerow(parse_report(report, i))
