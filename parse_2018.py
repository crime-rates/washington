import csv
import json
import pathlib
import urllib


CRIMES = (
    'Murder',
    'Manslaughter',
    'Rape',
    'Sodomy',
    'Sexual Assault w/Object',
    'Fondling',
    'Aggravated Assault',
    'Simple Assault',
    'Intimidation',
    'Kidnapping',
    'Incest',
    'Statutory Rape',
    'Human Trafficking Offenses',
    'Violation of No Contact/Protect.',
    'Robbery',
    'Burglary',
    'Larceny-Theft Offenses',
    'Motor Vehicle Theft',
    'Arson',
    'Destruction of Property',
    'Counterfeiting/Forgery',
    'Fraud Offenses',
    'Embezzlement',
    'Extortion/Blackmail',
    'Bribery',
    'Stolen Property Offenses',
    'Animal Cruelty',
    'Drug/Narcotic Violations',
    'Drug Equipment Violations',
    'Gambling Offenses',
    'Pornography',
    'Prostitution Offenses',
    'Weapon Law Violations',
)


FIRST_NIBRS_REPORTS = {
    int((211 - 91) / 2),
    int((217 - 91) / 2),
    int((225 - 91) / 2),
    int((227 - 91) / 2),
    int((231 - 91) / 2),
    int((243 - 91) / 2),
    int((251 - 91) / 2),
    int((257 - 91) / 2),
    int((259 - 91) / 2),
    int((273 - 91) / 2),
    int((275 - 91) / 2),
    int((279 - 91) / 2),
    int((281 - 91) / 2),
    int((289 - 91) / 2),
}


def parse_report(report, index):
    grid = {}
    for t in report['Texts']:
        x = round(t['x'], 2)
        y = round(t['y'], 2)

        grid[y] = grid.get(y, {})
        grid[y][x] = urllib.parse.unquote(t['R'][0]['T']).strip()

    rows = [
        [grid[y][x] for x in sorted(grid[y].keys())]
        for y in sorted(grid.keys())
    ]

    if index in FIRST_NIBRS_REPORTS:
        contracted = (
            'Sex Assault w/Object',
            'Viol. No Contact/Protect.',
        )
        crimes = {
            row[0]: row[1]
            for row in rows
            if row[0] in CRIMES + contracted
        }
        crimes['Sexual Assault w/Object'] = crimes['Sex Assault w/Object']
        del crimes['Sex Assault w/Object']

        crimes['Violation of No Contact/Protect.'] = crimes['Viol. No Contact/Protect.']
        del crimes['Viol. No Contact/Protect.']
    else:
        crimes = {
            row[0]: row[2]
            for row in rows
            if row[0] in CRIMES
        }

    try:
        if index in {60, 91}:
            response = dict(
                crimes,
                agency=rows[0][0],
                county=rows[0][1],
                population=rows[0][-1].replace(',', ''),
                months=rows[1][-1],
                year='2018',
            )

            if index == 60:
                response['Murder'] = '2'

            return response
        elif index == 4:
            return dict(
                crimes,
                agency='Asotin PD',
                county='Asotin County',
                population='1275',
                months='7',
                year='2018',
            )
        elif index == 22:
            return dict(
                crimes,
                agency='Vancouver PD',
                county='Clark County',
                population='183500',
                months='12',
                year='2018',
            )
        elif index == 49:
            return dict(
                crimes,
                agency='Cosmopolis PD',
                county='Grays Harbor County',
                population='1665',
                months='7',
                year='2018',
            )
        elif index == 55:
            return dict(
                crimes,
                agency='Westport PD',
                county='Grays Harbor County',
                population='2120',
                months='12',
                year='2018',
            )
        elif index == 69:
            return dict(
                crimes,
                agency='Clyde Hill PD',
                county='King County',
                population='3045',
                months='10',
                year='2018',
            )
        elif index == 83:
            return dict(
                crimes,
                agency='Muckleshoot Tribal PD',
                county='King County',
                population='0',
                months='5',
                year='2018',
            )
        elif index == 85:
            return dict(
                crimes,
                agency='Normandy Park PD',
                county='King County',
                population='6595',
                months='11',
                year='2018',
            )
        elif index == 110:
            return dict(
                crimes,
                agency='Kittitas PD',
                county='Kittitas County',
                population='1515',
                months='2',
                year='2018',
            )
        elif index == 121:
            return dict(
                crimes,
                agency='Toledo PD',
                county='Lewis County',
                population='720',
                months='10',
                year='2018',
            )
        elif index == 122:
            return dict(
                crimes,
                agency='Winlock PD',
                county='Lewis County',
                population='1340',
                months='8',
                year='2018',
            )
        elif index == 124:
            return dict(
                crimes,
                agency='Reardan PD',
                county='Lincoln County',
                population='575',
                months='8',
                year='2018',
            )
        elif index == 128:
            return dict(
                crimes,
                agency='Brewster PD',
                county='Okanogan County',
                population='2405',
                months='11',
                year='2018',
            )
        elif index == 158:
            return dict(
                crimes,
                agency='San Juan SO',
                county='San Juan County',
                population='16810',
                months='4',
                year='2018',
            )
        elif index == 164:
            return dict(
                crimes,
                agency='Skagit Drug Task Force',
                county='Skagit County',
                population='0',
                months='8',
                year='2018',
            )
        elif index == 165:
            return dict(
                crimes,
                agency='Swinomish Tribal PD',
                county='Skagit County',
                population='0',
                months='8',
                year='2018',
            )
        elif index == 202:
            return dict(
                crimes,
                agency='Tenino PD',
                county='Thurston County',
                population='2360',
                months='10',
                year='2018',
            )
        elif index == 205:
            return dict(
                crimes,
                agency='WA St Insurance Comm.',
                county='State Agency',
                population='0',
                months='8',
                year='2018',
            )
        elif index == 206:
            return dict(
                crimes,
                agency='Yelm PD',
                county='Thurston County',
                population='9030',
                months='11',
                year='2018',
            )
        elif index == 213:
            return dict(
                crimes,
                agency='Blaine PD',
                county='Whatcom County',
                population='5315',
                months='1',
                year='2018',
            )
        elif index == 214:
            return dict(
                crimes,
                agency='Everson PD',
                county='Whatcom County',
                population='4230',
                months='7',
                year='2018',
            )
        elif index == 216:
            return dict(
                crimes,
                agency='Lummi Nation PD',
                county='Whatcom County',
                population='0',
                months='8',
                year='2018',
            )
        elif index == 221:
            return dict(
                crimes,
                agency='Colfax PD',
                county='Whitman County',
                population='2820',
                months='7',
                year='2018',
            )
        elif index == 226:
            return dict(
                crimes,
                agency='Mabton PD',
                county='Yakima County',
                population='2315',
                months='7',
                year='2018',
            )
        elif index == 232:
            return dict(
                crimes,
                agency='Wapato PD',
                county='Yakima County',
                population='5040',
                months='2',
                year='2018',
            )
        else:
            if index in FIRST_NIBRS_REPORTS:
                return dict(
                    crimes,
                    agency=rows[0][0],
                    county=rows[1][1],
                    population=rows[0][2].replace(',', ''),
                    months=rows[2][3],
                    year='2018',
                )
            else:
                return dict(
                    crimes,
                    agency=rows[1][0],
                    county=rows[0][0],
                    population=rows[1][1].replace(',', ''),
                    months=rows[2][1],
                    year='2018',
                )
    except IndexError:
        print(index, rows[0:20])
        raise


reports = json.loads(
    pathlib.Path('decomposed/2018.json').read_text()
)['formImage']['Pages'][90:560:2]

with open('converted/2018.csv', 'w') as csvfile:
    writer = csv.DictWriter(csvfile, fieldnames=(
        'agency',
        'county',
        'population',
        'months',
        'year',
    ) + CRIMES)

    writer.writeheader()
    for i, report in enumerate(reports):
        writer.writerow(parse_report(report, i))
