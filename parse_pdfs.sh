#!/bin/bash

NODE_OPTIONS="--max-old-space-size=4096" pdf2json -f 2019.pdf -o decomposed -s -t -c
NODE_OPTIONS="--max-old-space-size=4096" pdf2json -f 2018.pdf -o decomposed -s -t -c
NODE_OPTIONS="--max-old-space-size=6144" pdf2json -f 2017.pdf -o decomposed -s -t -c
NODE_OPTIONS="--max-old-space-size=6144" pdf2json -f 2016.pdf -o decomposed -s -t -c
NODE_OPTIONS="--max-old-space-size=4096" pdf2json -f 2015.pdf -o decomposed -s -t -c
NODE_OPTIONS="--max-old-space-size=4096" pdf2json -f 2014.pdf -o decomposed -s -t -c
NODE_OPTIONS="--max-old-space-size=4096" pdf2json -f 2013.pdf -o decomposed -s -t -c
NODE_OPTIONS="--max-old-space-size=4096" pdf2json -f 2012.pdf -o decomposed -s -t -c
