import csv
import json
import pathlib
import urllib


CRIMES = (
    'Murder',
    'Manslaughter',
    'Rape',
    'Sodomy',
    'Sexual Assault w/Object',
    'Fondling',
    'Aggravated Assault',
    'Simple Assault',
    'Intimidation',
    'Kidnapping',
    'Incest',
    'Statutory Rape',
    'Human Trafficking Offenses',
    'Violation of No Contact/Protect.',
    'Robbery',
    'Burglary',
    'Larceny-Theft Offenses',
    'Motor Vehicle Theft',
    'Arson',
    'Destruction of Property',
    'Counterfeiting/Forgery',
    'Fraud Offenses',
    'Embezzlement',
    'Extortion/Blackmail',
    'Bribery',
    'Stolen Property Offenses',
    'Animal Cruelty',
    'Drug/Narcotic Violations',
    'Drug Equipment Violations',
    'Gambling Offenses',
    'Pornography',
    'Prostitution Offenses',
    'Weapon Law Violations',
)

START = 91
END = 552
P = lambda p: (p - (START + 1)) // 2

TITLES = {
    P(450): dict(
        agency='SPOKANE SO',
        county='Spokane',
        population='154470',
        months='7',
    ),
    P(460): dict(
        agency='SPOKANE PD',
        county='Spokane',
        population='215357',
        months='9',
    ),
    P(462): dict(
        agency='SPOKANE VALLEY PD',
        county='Spokane',
        population='94160',
        months='7',
    ),
}

FIRST_YEAR = {P(450), P(460), P(462)}


def make_rows(report):
    grid = {}
    for t in report['Texts']:
        x = round(t['x'], 1)
        y = round(t['y'], 1)

        grid[y] = grid.get(y, {})
        grid[y][x] = urllib.parse.unquote(t['R'][0]['T']).strip()

    rows = [
        [grid[y][x] for x in sorted(grid[y].keys())]
        for y in sorted(grid.keys())
    ]

    months_reported = None
    fixed_rows = []
    for row in rows:
        if len(row) > 2 and (row[0] + row[1]) in CRIMES:
            fixed_rows.append([row[0] + row[1]] + row[2:])
        elif len(row) > 2 and (row[0] + ' Offenses') in CRIMES:
            fixed_rows.append([row[0] + ' Offenses'] + row[1:])
        elif row[0] == 'Larceny Theft':
            fixed_rows.append(['Larceny-Theft Offenses'] + row[1:])
        elif row[0] == 'Months Reported:':
            months_reported = row[1]
        elif 'Population:' in row:
            if len(row) == 5:
                fixed_rows.append([row[0] + row[1]] + row[2:])
            else:
                fixed_rows.append(row)
        else:
            fixed_rows.append(row)

    return fixed_rows, months_reported


def parse_report(report, index):
    rows, months_reported = make_rows(report)

    crimes = {
        row[0]: (row[1] if index in FIRST_YEAR else row[2]).replace(',', '')
        for row in rows
        if row[0] in CRIMES
    }

    try:
        return dict(
            crimes,
            year='2016',
            **TITLES.get(index, dict(
                agency=rows[0][0],
                county=rows[0][1],
                population=rows[0][3].replace(',', ''),
                months=months_reported,
            )),
        )
    except IndexError:
        print(index, rows[0:20])
        raise


reports = json.loads(
    pathlib.Path('decomposed/2016.json').read_text()
)['formImage']['Pages'][START:END:2]


with open('converted/2016.csv', 'w') as csvfile:
    writer = csv.DictWriter(csvfile, fieldnames=(
        'agency',
        'county',
        'population',
        'months',
        'year',
    ) + CRIMES)

    writer.writeheader()
    for i, report in enumerate(reports):
        writer.writerow(parse_report(report, i))
