#!/bin/bash

wget -O 2019.pdf https://www.waspc.org/assets/CJIS/crime%20in%20washington%202019-small.pdf
wget -O 2018.pdf https://waspc.memberclicks.net/assets/CJIS/2018%20ciw.pdf
wget -O 2017.pdf https://waspc.memberclicks.net/assets/CJIS/crime%20in%20washington%202017.small.pdf
wget -O 2016.pdf https://waspc.memberclicks.net/assets/CJIS/2016%20crime%20in%20washington.small.pdf
wget -O 2015.pdf https://waspc.memberclicks.net/assets/CJIS/crime%20in%20washington%202015.small.pdf
wget -O 2014.pdf https://waspc.memberclicks.net/assets/CJIS/ciw%202014%20small.pdf
wget -O 2013.pdf https://waspc.memberclicks.net/assets/CJIS/2013%20ciw.pdf
wget -O 2012.pdf https://waspc.memberclicks.net/assets/CJIS/2012%20ciw.pdf
wget https://www.waspc.org/assets/CJIS/1984-2011.xlsx
