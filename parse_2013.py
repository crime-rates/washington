import csv
import functools
import json
import operator
import pathlib
import urllib


CRIMES = (
    'Murder',
    'Manslaughter',
    'Forcible Rape',
    'Forcible Sodomy',
    'Sexual Assault w/Object',
    'Forcible Fondling',
    'Aggravated Assault',
    'Simple Assault',
    'Intimidation',
    'Kidnapping',
    'Incest',
    'Statutory Rape',
    'Human Trafficking',
    'Violation of No Contact/Protect.',
    'Robbery',
    'Burglary',
    'Larceny Theft',
    'Motor Vehicle Theft',
    'Arson',
    'Destruction of Property',
    'Counterfeiting/Forgery',
    'Fraud',
    'Embezzlement',
    'Extortion/Blackmail',
    'Bribery',
    'Stolen Property Offenses',
    'Drug/Narcotic Violations',
    'Drug Equipment Violations',
    'Gambling Offenses',
    'Pornography',
    'Prostitution',
    'Weapon Law Violations',
)

ALT_CRIMES = {
    'Sex Assault w/Object': 'Sexual Assault w/Object',
    'Viol. No Contact/Protect.': 'Violation of No Contact/Protect.',
    'Counterfieting/Forgery': 'Counterfeiting/Forgery',
}

START = 87
END = 518
P = lambda p: (p - (START + 1)) // 2

TITLES = {
    # P(409): dict(
        # agency='SNOHOMISH SO',
        # county='Snohomish',
        # population='337275',
        # months='10',
    # ),
    # P(411): dict(
        # agency='ARLINGTON PD',
        # county='Snohomish',
        # population='18490',
        # months='10',
    # ),
    # P(415): dict(
        # agency='EDMONDS PD',
        # county='Snohomish',
        # population='40490',
        # months='10',
    # ),
    # P(417): dict(
        # agency='EVERETT PD',
        # county='Snohomish',
        # population='105800',
        # months='10',
    # ),
    # P(419): dict(
        # agency='GRANITE FALLS PD',
        # county='Snohomish',
        # population='3390',
        # months='10',
    # ),
    P(238): dict(
        agency='KENT PD',
        county='King County',
        population='120500',
        months='8',
    ),
    P(442): dict(
        agency='LACEY PD',
        county='Thurston County',
        population='44350',
        months='5',
    ),
    P(444): dict(
        agency='OLYMPIA PD',
        county='Thurston County',
        population='48480',
        months='5',
    ),
    P(450): dict(
        agency='TUMWATER PD',
        county='Thurston County',
        population='18300',
        months='5',
    ),
    P(456): dict(
        agency='YELM PD',
        county='Thurston County',
        population='7470',
        months='4',
    ),
}

FIRST_YEAR = {
    P(238), P(442), P(444), P(450), P(456),
}


def make_rows(report):
    grid = {}
    for t in report['Texts']:
        x = round(t['x'], 1)
        y = round(t['y'], 1)

        grid[y] = grid.get(y, {})
        grid[y][x] = urllib.parse.unquote(t['R'][0]['T']).strip()

    rows = [
        [grid[y][x] for x in sorted(grid[y].keys())]
        for y in sorted(grid.keys())
    ]

    months_reported = None
    fixed_rows = []
    for row in rows:
        if len(row) >= 2:
            candidate = None
            rest = None

            for i in range(len(row)):
                possible = functools.reduce(operator.add, row[0:i + 1])
                if possible in CRIMES:
                    candidate = possible
                    rest = row[i + 1:]
            else:
                if (row[0] + ' Offenses') in CRIMES:
                    candidate = row[0] + ' Offenses'
                    rest = row[1:]
                elif row[0] in ALT_CRIMES:
                    candidate = ALT_CRIMES[row[0]]
                    rest = row[1:]
                elif (row[0] + row[1]) in ALT_CRIMES:
                    candidate = ALT_CRIMES[row[0] + row[1]]
                    rest = row[2:]
                elif row[0] == 'Months Reported:':
                    months_reported = row[1]
                    continue
                elif 'Population:' in row:
                    if len(row) == 5:
                        fixed_rows.append([row[0] + row[1]] + row[2:])
                    else:
                        fixed_rows.append(row)
                    continue

            if candidate is not None:
                fixed_rows.append([candidate] + rest)
                continue
        else:
            fixed_rows.append(row)

    return fixed_rows, months_reported


def parse_report(report, index):
    rows, months_reported = make_rows(report)

    crimes = {
        row[0]: (row[1] if index in FIRST_YEAR else row[2]).replace(',', '')
        for row in rows
        if row[0] in CRIMES
    }

    try:
        return dict(
            crimes,
            year='2013',
            **TITLES.get(index, dict(
                agency=rows[0][0],
                county=rows[0][1],
                population=rows[0][3].replace(',', ''),
                months=months_reported,
            )),
        )
    except IndexError:
        print(index, rows[0:20])
        raise


reports = json.loads(
    pathlib.Path('decomposed/2013.json').read_text()
)['formImage']['Pages'][START:END:2]


with open('converted/2013.csv', 'w') as csvfile:
    writer = csv.DictWriter(csvfile, fieldnames=(
        'agency',
        'county',
        'population',
        'months',
        'year',
    ) + CRIMES)

    writer.writeheader()
    for i, report in enumerate(reports):
        writer.writerow(parse_report(report, i))
