## Dependencies

* `wget`
* `pdf2json`
* `gnumeric`
* Python 3.8+

## Steps

1. `./grab_pdfs.sh`
2. `./parse_pdfs.sh`
3. Manually generate CSV from Excel file.
4. Run all parsers - e.g. `python3 parse_2019.py`
