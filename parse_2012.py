import csv
import functools
import json
import operator
import pathlib
import urllib


CRIMES = (
    'Murder',
    'Negligent Manslaughter',
    'Forcible Rape',
    'Forcible Sodomy',
    'Sex Assault w/Object',
    'Forcible Fondling',
    'Robbery',
    'Aggravated Assault',
    'Simple Assault',
    'Intimidation',
    'Burglary',
    'Larceny',
    'Motor Vehicle Theft',
    'Arson',
    'Kidnapping',
    'Bribery',
    'Counterfeiting/Forgery',
    'Vandalism',
    'Drug/Narcotics',
    'Drug Equipment',
    'Embezzlement',
    'Extortion/Blackmail',
    'Fraud',
    'Gambling',
    'Pornography',
    'Prostitution',
    'Incest',
    'Statutory Rape',
    'Stolen Property',
    'Viol. No Contact/Protect.',
    'Weapons Law Violation',
)

ALT_CRIMES = {
    'Contact/Protect.': 'Viol. No Contact/Protect.',
}

START = 86
END = 479
P = lambda p: (p - (START + 1)) // 2

TITLES = {}

FIRST_YEAR = {}


def make_rows(report):
    grid = {}
    for t in report['Texts']:
        x = round(t['x'], 1)
        y = round(t['y'], 1)

        grid[y] = grid.get(y, {})
        grid[y][x] = urllib.parse.unquote(t['R'][0]['T'])

    rows = [
        [grid[y][x] for x in sorted(grid[y].keys())]
        for y in sorted(grid.keys())
    ]

    agency = ''.join(rows[1])
    if agency.endswith('Sheriff’s Office'):
        agency = agency[0:-len('Sheriff’s Office')].strip() + ' SO'
    elif agency.endswith('Police Department'):
        agency = agency[0:-len('Police Department')].strip() + ' PD'

    both = ''.join([fragment.strip() for fragment in rows[2]])
    months = None
    population = None

    if both.startswith('Months Reported:'):
        both = both.replace('Months Reported:', '').replace(', ', '').replace(',', '')
        try:
            months, population = both.replace('Population:', ' ').split()
        except ValueError:
            print(agency, both)
            print(rows[2])
            raise
    else:
        raise Exception('broken months')

    return grid, agency, months, population

NON_STANDARD = [
    P(pp) for pp in
    [91, 95, 105, 117, 119, 121, 127, 129, 133, 137, 141, 145, 147, 149, 151,
    161, 163, 165, 169, 171, 179, 181, 185, 191, 199, 207, 211, 215, 219, 221,
    223, 225, 227, 231, 233, 235, 237, 243, 245, 249, 251, 253, 257, 259, 263,
    271, 275, 277, 279, 281, 285, 297, 299, 303, 317, 323, 327, 365, 367, 369,
    371, 373, 375, 377, 399, 407, 413, 423, 429, 431, 435, 439, 441, 447, 457,
    459, 461, 463, 467, 471, 473, 475, 477]
]

def parse_report(report, index):
    grid, agency, months, population = make_rows(report)

    crime_rows = []
    for key, value in grid.items():
        # y is in correct range here
        if 12 < key < 43:
            row = []
            last_coordinate = 0
            for coordinate in sorted(value):
                if coordinate < 10:
                    row.append(value[coordinate])
                elif 10 <= coordinate <= 14:
                    continue
                elif 14 < coordinate < 17.5:
                    row.append(value[coordinate])
                    break
                else:
                    row.append('0')
                last_coordinate = coordinate
            crime_rows.append(row)

    crimes = {}
    for row in crime_rows:
        first_num = 0
        for i, c in enumerate(row):
            try:
                int(c.replace(',', ''))
                first_num = i
            except ValueError:
                continue

        nospace = ''.join(row[:i])
        space = ' '.join(row[:i])
        if space in CRIMES:
            crimes[space] = row[i].replace(',', '')
        elif nospace in CRIMES:
            crimes[nospace] = row[i].replace(',', '')
        elif alt_crime := ALT_CRIMES.get(space):
            crimes[alt_crime] = row[i].replace(',', '')
        elif alt_crime := ALT_CRIMES.get(nospace):
            crimes[alt_crime] = row[i].replace(',', '')

    non_standard_crimes = {}
    for row in grid.values():
        first, *rest = list(row.items())
        _, crime = first

        if crime in CRIMES:
            pass
        elif crime in ALT_CRIMES:
            crime = ALT_CRIMES[crime]
        else:
            crime = None

        if crime is not None:
            if rest:
                coord, count = rest[0]
                if coord < 16:
                    non_standard_crimes[crime] = count.replace(',', '')
                else:
                    non_standard_crimes[crime] = 0

    for crime in CRIMES:
        non_standard_crimes[crime] = non_standard_crimes.get(crime, 0)
        crimes[crime] = crimes.get(crime, 0)

    try:
        return dict(
            **(
                non_standard_crimes
                if index in NON_STANDARD
                else crimes
            ),
            year='2012',
            **TITLES.get(index, dict(
                agency=agency,
                months=months,
                population=population,
            )),
        )
    except IndexError:
        print(index, rows[0:20])
        raise


reports = json.loads(
    pathlib.Path('decomposed/2012.json').read_text()
)['formImage']['Pages'][START:END:2]


with open('converted/2012.csv', 'w') as csvfile:
    writer = csv.DictWriter(csvfile, fieldnames=(
        'agency',
        'population',
        'months',
        'year',
    ) + CRIMES)

    writer.writeheader()
    for i, report in enumerate(reports):
        writer.writerow(parse_report(report, i))
