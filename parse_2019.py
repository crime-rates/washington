import csv
import json
import pathlib
import urllib

CRIMES = (
    'Murder',
    'Manslaughter',
    'Rape',
    'Sodomy',
    'Sexual Assault w/Object',
    'Fondling',
    'Aggravated Assault',
    'Simple Assault',
    'Intimidation',
    'Kidnapping',
    'Incest',
    'Statutory Rape',
    'Human Trafficking Offenses',
    'Violation of No Contact/Protect.',
    'Robbery',
    'Burglary',
    'Larceny-Theft Offenses',
    'Motor Vehicle Theft',
    'Arson',
    'Destruction of Property',
    'Counterfeiting/Forgery',
    'Fraud Offenses',
    'Embezzlement',
    'Extortion/Blackmail',
    'Bribery',
    'Stolen Property Offenses',
    'Animal Cruelty',
    'Drug/Narcotic Violations',
    'Drug Equipment Violations',
    'Gambling Offenses',
    'Pornography',
    'Prostitution Offenses',
    'Weapon Law Violations',
)


def parse_report(report):
    grid = {}
    for t in report['Texts']:
        x = round(t['x'], 2)
        y = round(t['y'], 2)

        grid[y] = grid.get(y, {})
        grid[y][x] = urllib.parse.unquote(t['R'][0]['T']).strip()

    rows = [
        [grid[y][x] for x in sorted(grid[y].keys())]
        for y in sorted(grid.keys())
    ]

    try:
        return dict(
            {
                row[0]: row[2].replace(',', '')
                for row in rows
                if row[0] in CRIMES
            },
            agency=rows[0][0],
            county=rows[0][1],
            population=rows[0][3].replace(',', ''),
            months=rows[1][1],
            year='2019',
        )
    except IndexError:
        # Only Woodinville fails.
        return dict(
            {
                row[0]: row[2]
                for row in rows
                if row[0] in CRIMES
            },
            agency=rows[0][0],
            county='King County',
            population='12410',
            months='12',
            year='2019',
        )


reports = json.loads(
    pathlib.Path('decomposed/2019.json').read_text()
)['formImage']['Pages'][96:562:2]

with open('converted/2019.csv', 'w') as csvfile:
    writer = csv.DictWriter(csvfile, fieldnames=(
        'agency',
        'county',
        'population',
        'months',
        'year',
    ) + CRIMES)

    writer.writeheader()
    for report in reports:
        writer.writerow(parse_report(report))
